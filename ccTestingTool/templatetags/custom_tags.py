import json
from django import template

register = template.Library()

@register.filter(is_safe=True)
def serializeUrl(url):
    suburl = "" if not url.suburl else url.suburl
    return json.dumps({
        "id": url.id,
        "request_type": url.request_type,
        "name": url.name,
        "url": url.parent.url + suburl,
        "params": json.loads(url.default_params),
    })


@register.filter(is_safe=True)
def serializeData(data):
    return json.dumps(data)

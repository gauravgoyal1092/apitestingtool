django.jQuery(document).ready(function() {
	var $ = window.$;

	/* Work around for dynamic added html in admin panel. 
		When user click on add new inline then run jsoneditor
		".add-row" is div class of the link and consistent throughout the admin panel.
	 */
	 $(".add-row").on("click", function (){
	     IntegrateJsonEditorWithNewRow();
	 });

	 $(".jsonfield-textarea").each(function(){

		// variable elements for better performance
		var $this = $(this);
		var parent_div = $this.parent("div");

	    // True if current div is not of the base template.
	    if (! $this.parents(".inline-related").hasClass("last-related")){
	    	// Integrate json editor
	    	integrate_json_editor($this, parent_div);
	    }
	});

	 $('.submit-row input[type=submit]').on("click", function(e){
	 	for(j=json_editor_id-1; j >0 ; j--){
	 		
			// On save get data from json editor and update in Textarea. Django will automatically pick updated details from textarea.
			var containerId = "#custom-id-json-editor"+ j;
			var editor = editor_set[containerId];
			var textarea = $(editor["container"]).parent().find("textarea");
			// save data to json field
			save_data_to_json_field(editor, textarea);
		}
	});

	});

function integrate_json_editor($this, parent_div){

	// Check if value in Textarea is valid json
	var json_obj = get_data_from_json_field($this);


		// If valid json hide div of Textarea and create a sibling div for jsoneditor container.
		parent_div.hide();
		var label = parent_div.find("label").text();
		$this.parent().parent().append('<label for=""custom-id-json-editor'+json_editor_id+'" class="required" style="padding-right: 0px;">'+label+'</label><div id="custom-id-json-editor'+json_editor_id+'" style="width: 95%;float: right;"></div>')
		
		var containerId = "#custom-id-json-editor"+ json_editor_id;
		var container = $(containerId)[0];
		// object of json editor
		var editor = new JSONEditor(container);

		// Set json data in json editor
		set_data_in_json_editor(editor, json_obj)

		// Add object in dictionary so we can use it later to save or edit data.
		editor_set[containerId] = editor;
		json_editor_id = json_editor_id+1;
	
        $this.get(0).editor = editor;
}

function get_data_from_json_editor(editor){
	var json = editor.get();
	return json;
}

function set_data_in_json_editor(editor, data){
	if (typeof data == "string" && IsJsonString(data)){
	    data = JSON.parse(data);
	}

	editor.set(data);
}

function get_data_from_json_field(field_obj){
	try{
		if(field_obj.val()== ""){
			return "";
		}
		var json = jQuery.parseJSON(field_obj.val());
		return json;
	}catch(err){
		return false;
	}
}

function save_data_to_json_field(editor, textarea){
	data = get_data_from_json_editor(editor);
    // Save in textarea 
    textarea.val(JSON.stringify(data, null, 2));
}

var editor_set = {}
var json_editor_id = 1;

window.IntegrateJsonEditorWithNewRow = function(){

	var $ = window.$;
	$(".jsonfield-textarea").each(function(){
		// variable elements for better performance
		var $this = $(this);
		var parent = $this.parents(".inline-related");
		var parent_div = $this.parent("div");

	    // If dynamically added row but is not template itself and its json field is still visible (so we will know that it is newly added row).
	    if (parent.hasClass("last-related") && !parent.hasClass("empty-form") && parent_div.is(":visible")){

	    	// Integrate json editor
	    	integrate_json_editor($this, parent_div);
	    }
	});

}

function IsJsonString(str) {
	// In case the json value is only a string ex : "test"
	// we get error as we are trying to convert string to object.
	// applied try and catch to handle this error, So if in case of error same data will be passed
	// to the editor instance.
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}


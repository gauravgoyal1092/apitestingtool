
from django.conf.urls import url
from django.contrib import admin

from django.conf.urls.static import static

import ccTestingTool.settings as settings
import testingTool.views

urlpatterns = [
    url(r'^$', testingTool.views.index, name='index'),
    url(r'^feedback/', testingTool.views.feedback, name='feedback'),
    url(r'^docs/', testingTool.views.docs, name='docs'),
    url(r'^admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from testingTool.models import (Domain, Request, ParentModel,
                                Response, Feedback, Documentation)
import ccTestingTool.settings as settings


class ResponseAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user_type', 'response_status')
    def formfield_for_dbfield(self, db_field, **kwargs):

        field = super(ResponseAdmin, self).formfield_for_dbfield(db_field, **kwargs)
        if (db_field.name == 'request_object' or
            db_field.name == 'response' or db_field.name == 'user_information'):
            field.widget.attrs['class'] = "jsonfield-textarea"

        return field

    class Media:
        js = [
            settings.STATIC_URL + 'admin/js/vendor/jquery-1.9.1.min.js',
            settings.STATIC_URL + 'admin/js/vendor/jsoneditor/dist/jsoneditor.min.js',
            settings.STATIC_URL + 'admin/js/admin-jsonfield.js'
        ]
        css = {
            "all": (settings.STATIC_URL + 'admin/js/vendor/jsoneditor/dist/jsoneditor.min.css',)
        }


admin.site.register(Domain)
admin.site.register(ParentModel)
admin.site.register(Request)
admin.site.register(Feedback)
admin.site.register(Response, ResponseAdmin)
admin.site.register(Documentation)

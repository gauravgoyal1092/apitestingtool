# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Domain(models.Model):
    name = models.CharField(max_length=300)
    default = models.BooleanField(default=False)
    url = models.URLField(null=False, blank=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.default:
            Domain.objects.filter(default=True).update(default=False)
        return super(Domain, self).save(*args, **kwargs)


class ParentModel(models.Model):
    name = models.CharField(max_length=300, unique=True)
    url = models.CharField(max_length=300)
    order = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.name


class Request(models.Model):
    request_type_choices = (
        ('get', 'GET'),
        ('post', 'POST'),
        ('put', 'PUT'),
        ('delete', 'DELETE'),
    )

    name = models.CharField(max_length=300)
    parent = models.ForeignKey(ParentModel)
    request_type = models.CharField(
        max_length=30, choices=request_type_choices, default="get")
    default_params = models.TextField(default={})
    suburl = models.CharField(max_length=100, null=True, blank=True)
    order = models.PositiveIntegerField(default=1)
    details = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Response(models.Model):
    request_object = models.TextField(default={})
    response = models.TextField(default={})
    user_type = models.CharField(max_length=300)
    user_information = models.TextField(default={})
    response_status = models.CharField(max_length=300)


class Feedback(models.Model):
    request_type_choices = (
        ('good', 'Good'),
        ('bad', 'Bad'),
        ('discuss', 'Will have to discuss'),
    )
    response = models.ForeignKey(Response)
    feedback = models.CharField(
        max_length=30, choices=request_type_choices, default="good")
    information = models.TextField(default="")


class Documentation(models.Model):
    request_id = models.PositiveIntegerField(default=0)
    request_type = models.CharField(max_length=300)
    request_url = models.CharField(max_length=300)
    user_type = models.CharField(max_length=300)
    response = models.CharField(max_length=300)

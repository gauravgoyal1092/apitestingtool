# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-30 03:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testingTool', '0014_request_details'),
    ]

    operations = [
        migrations.AddField(
            model_name='documentation',
            name='request_id',
            field=models.PositiveIntegerField(default=0),
        ),
    ]

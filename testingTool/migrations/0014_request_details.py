# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-30 03:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testingTool', '0013_auto_20190129_0604'),
    ]

    operations = [
        migrations.AddField(
            model_name='request',
            name='details',
            field=models.TextField(blank=True, null=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
import json
import requests
import urlparse
import pickle

from django.shortcuts import render
from testingTool.models import Domain, Request, ParentModel, Response, Feedback, Documentation
from django.http import HttpResponse


def get_domain(url):
    return url.split("//")[-1].split("/")[0]


def requestCCLogin(url, request_type, params, request):
    session = requests.session()
    filename = get_domain(url)

    if request_type == "post":
        r = session.post(url, data=params)
    elif request_type == "get":
        try:
            with open(filename, 'rb') as f:
                session.cookies.update(pickle.load(f))
        except:
            pass
        r = session.get(url, params=params)
    content = r.content

    if os.path.isfile(filename):
        os.remove(filename)

    print content
    if "csrftoken" in content:
        request.session['my_data'] = r.content
    with open(filename, 'wb') as f:
        pickle.dump(session.cookies, f)
    return r


def requestCCLogout(url, request_type, params, request):
    session = requests.session()
    filename = get_domain(url)
    try:
        with open(filename, 'rb') as f:
            session.cookies.update(pickle.load(f))
    except:
        pass
    r = session.get(url, params=params)

    if "my_data" in request.session:
        del request.session['my_data']
    if os.path.isfile(filename):
        os.remove(filename)
    return r


def requestToCC(url, request_type, params, request):
    session = requests.session()
    filename = get_domain(url)
    try:
        with open(filename, 'rb') as f:
            session.cookies.update(pickle.load(f))
    except:
        pass
    base_url = urlparse.urljoin(url, '/')

    if "csrftoken" in session.cookies:
        session.headers.update({
            'referer': base_url,
            'X-CSRFToken': session.cookies["csrftoken"]
        })
    if request_type == "get":
        r = session.get(url, params=params)
    elif request_type == "post":
        r = session.post(url, data=params)
    elif request_type == "put":
        r = session.put(url, data=params)
    elif request_type == "delete":
        r = session.delete(url)
    return r


def index(request):
    my_data = {"user": {"roles": ["unauthenticated"]}}
    if "my_data" in request.session:
        my_data = json.loads(request.session['my_data'])

    if request.method == "POST":
        url = request.POST.get("url")
        request_type = request.POST.get("request-type")
        request_id = request.POST.get("request_id", 0)

        data = request.POST.items()
        params = {}
        indexes = []
        for key, values in data:
            if "key" in key:
                indexes.append(int(key.split("-")[1]))
        if indexes:
            for index in indexes:
                params[request.POST.get("key-"+str(index))] = request.POST.get("value-"+str(index))

        if "login" in url or "status" in url:
            response = requestCCLogin(url, request_type, params, request)
            my_data = json.loads(request.session["my_data"])
        elif "logout" in url:
            response = requestCCLogout(url, request_type, params, request)
            my_data = json.loads(response.content)
        else:
            response = requestToCC(url, request_type, params, request)

        result = {}

        if "user" not in my_data or len(my_data["user"]["roles"]) == 0:
            roles = "unauthenticated"
        else:
            roles = ",".join(my_data["user"]["roles"])

        result["user"] = {"group": roles}
        result["url"] = url
        result["type"] = request_type
        result["params"] = params
        result["status_code"] = response.status_code
        result["json"] = json.dumps(response.content)

        docs = Documentation.objects.filter(request_id=request_id, request_type=request_type, user_type=roles).first()

        response_obj = Response.objects.create(
            response=result["json"],
            response_status=result["status_code"],
            user_type=roles,
            user_information=my_data["user"] if "user" in my_data else {},
            request_object=json.dumps({
                "id": request_id,
                "url": url,
                "params": params,
                "type": request_type,
            }),
        )

        return render(request, 'result.html', {
            'response_id': response_obj.id,
            'request': result,
            "params_json": json.dumps({
                'url': url,
                'params': params,
                'type': request_type
            }),
            "docs": docs
            }
        )

    urls = []
    models = ParentModel.objects.all().order_by('order')
    for m in models:
        urls.append({m.name: Request.objects.filter(parent=m)})

    request_type_choices = ["get", "post", "put", "delete"]

    return render(request, 'index.html', {
        'urls': urls,
        'domains': Domain.objects.filter(default=False),
        'default_domain': Domain.objects.filter(default=True).first(),
        'request_type_choices': request_type_choices,
        'my_data': my_data,
    })


def feedback(request):

    if request.method == "POST":
        feedback = request.POST.get("feedback")
        response_id = request.POST.get("response_id")
        information = request.POST.get("information")

        feedback_obj = Feedback.objects.filter(response__id=response_id)

        if not feedback_obj.exists():
            Feedback.objects.create(
                response=Response.objects.filter(id=response_id).first(),
                feedback=feedback,
                information=information
            )
        else:
            feedback_obj.update(information=information, feedback=feedback)

        return HttpResponse(json.dumps({"status": "Done"}), content_type="application/json")

    return HttpResponse(json.dumps({"status": "Get not allowed"}), content_type="application/json")


def docs(request):

    if request.method == "POST":
        url = request.POST.get("url")
        request_type = request.POST.get("type")
        user = request.POST.get("user")
        response = request.POST.get("response")
        request_id = request.POST.get("request_id")

        doc_obj = Documentation.objects.filter(request_id=request_id, request_type=request_type, user_type=user)

        if not doc_obj.exists():
            Documentation.objects.create(
                response=response,
                request_type=request_type,
                user_type=user,
                request_url=url,
            )
        else:
            doc_obj.update(response=response)

        return HttpResponse(json.dumps({"status": "Done"}), content_type="application/json")

    return HttpResponse(json.dumps({"status": "Get not allowed"}), content_type="application/json")

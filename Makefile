setup: createVirtualenv install

createVirtualenv:
	virtualenv virtualenv

install:
	. ./virtualenv/bin/activate && pip install -r requirements.txt

clean:
	rm -rf virtualenv
	@echo "Cleaned"

run:
	. ./virtualenv/bin/activate && python manage.py runserver 0.0.0.0:8001
